##=====================================================================================================##
########################## Synopsys Design Constraints (SDC) file information ###########################
##=====================================================================================================##
##
## Company: CERN (BE-BI)
##
## File Name: SGefe_System_Timing.sdc
##
## File versions history:
##
##       DATE          VERSION      AUTHOR             DESCRIPTION
##     - 16/03/2018    3.1          M. Barros Marin    Renamed differential ports
##     - 14/02/2018    3.0          M. Barros Marin    Updated to S-GEFE
##     - 23/11/2016    2.0          M. Barros Marin    Only system timing constraints
##     - 01/11/2016    1.6          M. Barros Marin    Cosmetic modifications
##     - 03/03/2016    1.5          M. Barros Marin    Cosmetic modifications
##     - 29/10/2015    1.0          M. Barros Marin    First .pdc file definition
##
## Libero version: v11.8
##
## Input Netlist Format: edif
##
## Targeted device:
##
##     - Vendor: Microsemi
##     - Model:  ProASIC3E(A3PE3000)/ProASIC3L(A3PE3000L) - 896 FBGA
##
## Description:
##
##     System timing SDC file for the Split - GBT-based Expandable Front-End (S-GEFE)
##     (L-GEFE + C-GEFE), the standard rad-hard digital board for CERN BE-BI applications.
##
## !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
## !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! IMPORTANT !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
## !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
## !!                                                                                                  !!
## !!                             Do not modify the content of this file                               !!
## !!                                                                                                  !!
## !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
## !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
##
##=====================================================================================================##
#########################################################################################################
##=====================================================================================================##

# Clocks

create_clock -name {Osc25Mhz}     -period 40.000000 -waveform {0.000000 20.000000} [get_ports {Osc25Mhz_ik}]
create_clock -name {GbtxClockDes} -period 25.000000 -waveform {0.000000 12.500000} [get_ports {GbtxClockDesP_ik}]

set_clock_groups -asynchronous -group [get_clocks {Osc25Mhz}]
set_clock_groups -asynchronous -group [get_clocks {GbtxClockDes}]

# False Paths Between Clocks

# False Path Constraints

# Maximum Delay Constraints

# Multicycle Constraints

# Virtual Clocks

# Output Load Constraints

# Driving Cell Constraints

# Wire Loads

# set_wire_load_mode top

# Other Constraints
