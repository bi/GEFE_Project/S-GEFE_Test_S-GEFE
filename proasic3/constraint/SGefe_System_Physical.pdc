##=====================================================================================================##
########################## Physical Design Constraints (PDC) file information ###########################
##=====================================================================================================##
##
## Company: CERN (BE-BI)
##
## File Name: SGefe_System_Physical.pdc
##
## File versions history:
##
##       DATE          VERSION      AUTHOR             DESCRIPTION
##     - 16/03/2018    3.1          M. Barros Marin    Renamed differential ports
##     - 14/02/2018    3.0          M. Barros Marin    Updated to S-GEFE
##     - 23/11/2016    2.0          M. Barros Marin    Only system physical constraints
##     - 01/11/2016    1.6          M. Barros Marin    Cosmetic modifications
##     - 22/05/2016    1.6          M. Barros Marin    Bug fixed in pin constraint FmcHa_iob24p[0]
##     - 03/03/2016    1.5          M. Barros Marin    Cosmetic modifications
##     - 23/10/2015    1.0          M. Barros Marin    First .pdc file definition
##
## Libero version: v11.8
##
## Input Netlist Format: edif
##
## Targeted device:
##
##     - Vendor: Microsemi
##     - Model:  ProASIC3E(A3PE3000)/ProASIC3L(A3PE3000L) - 896 FBGA
##
## Description:
##
##     System physical PDC file for the Split - GBT-based Expandable Front-End (S-GEFE)
##     (L-GEFE + C-GEFE), the standard rad-hard digital board for CERN BE-BI applications.
##
## !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
## !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! IMPORTANT !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
## !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
## !!                                                                                                  !!
## !!                             Do not modify the content of this file                               !!
## !!                                                                                                  !!
## !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
## !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
##
##=====================================================================================================##
#########################################################################################################
##=====================================================================================================##

##==========================================  I/O banks setting  ======================================##

##==== E-links banks (P2v5) ====##

set_iobank Bank0 -vcci 2.50 -fixed yes
set_iobank Bank5 -vcci 2.50 -fixed yes
set_iobank Bank6 -vcci 2.50 -fixed yes
set_iobank Bank7 -vcci 2.50 -fixed yes

##===========================================  I/O constraints  =======================================##

##==== Resets scheme ====##

## General reset:
## Comment: See Microsemi application note AC380.
set_io     {FpgaReset_ira}    -pinname T28   -fixed YES -iostd LVCMOS25 -register NO -res_pull NONE -schmitt_trigger OFF -in_delay OFF

##==== GBTx ====##

## Clocks:
## Comment: - GbtxClockDes is the clock used by the GBTx I/O registers.
##            GbtxClockDes is connected to the "Chip global" (Cg) clock network.
##            GbtxClockDes[1] from L-GEFE goes directly to the FMC connector bypassing the ProAsic3 FPGA.
##--
set_io     {GbtxClockDesP_ik} -pinname R2   -fixed YES -iostd LVDS -register NO -in_delay OFF
## Comment: GbtxClockDesN_ik  -pinname R5

##==== Miscellaneous ====##

## Crystal oscillator (25MHz):
## Comment: Osc25Mhz accesses the FPGA as a standard user I/O and then is forwarded to the "Chip global" (Cg) clock
## network though an internal clock buffer.
set_io     {Osc25Mhz_ik}      -pinname R1   -fixed YES -iostd LVCMOS25 -register NO -schmitt_trigger OFF -in_delay OFF
