# Top Level Design Parameters

# Clocks

create_clock -name {GbtxElinksDclk} -period 25.000000 -waveform {0.000000 12.500000} GbtxElinksDclkP_ik
create_clock -name {ClkFeedback0} -period 40.000000 -waveform {0.000000 20.000000}  {ClkFeedbackP_ikb2[0]}
create_clock -name {ClkFeedback1} -period 25.000000 -waveform {0.000000 12.500000}  {ClkFeedbackP_ikb2[1]}
create_clock -name {Osc25Mhz} -period 40.000000 -waveform {0.000000 20.000000} Osc25Mhz_ik
create_clock -name {GbtxClockDes} -period 25.000000 -waveform {0.000000 12.500000} GbtxClockDesP_ik

# False Paths Between Clocks


# False Path Constraints


# Maximum Delay Constraints


# Multicycle Constraints


# Virtual Clocks
# Output Load Constraints
# Driving Cell Constraints
# Wire Loads
# set_wire_load_mode top

# Other Constraints
