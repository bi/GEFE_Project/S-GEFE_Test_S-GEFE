//============================================================================================\\
//##################################   Module Information   ##################################\\
//============================================================================================\\
//
// Company: CERN (BE-BI)
//
// File Name: SGefeApplication.v
//
// File versions history:
//
//       DATE          VERSION            AUTHOR             DESCRIPTION
//     - 21/03/2018    S-GEFE Test 1.0    M. Barros Marin    Renamed differential ports
//     - 16/03/2018    2.1                M. Barros Marin    Renamed differential ports
//     - 14/02/2018    2.0                M. Barros Marin    Updated to S-GEFE
//     - 06/11/2016    1.6                M. Barros Marin    Cosmetic modifications
//     - 03/03/2016    1.5                M. Barros Marin    Cosmetic modifications
//     - 06/10/2015    1.0                M. Barros Marin    First .v module definition
//
// Language: SystemVerilog 2012
//
// Targeted device:
//
//     - Vendor: Microsemi
//     - Model:  ProASIC3E(A3PE3000)/ProASIC3L(A3PE3000L) - 896 FBGA
//
// Description:
//
//     Generic HDL "application" module for the Split - GBT-based Expandable Front-End (S-GEFE)
//     (L-GEFE + C-GEFE), the standard rad-hard digital board for CERN BE-BI applications.
//
//============================================================================================\\
//############################################################################################\\
//============================================================================================\\

`timescale 1ns/100ps

module SGefeApplication
//========================================  I/O ports  =======================================\\
(
    //==== Resets Scheme ====\\

    // Opto Loss Of Signal (LOS) reset:
    // Comment: This reset is asserted when there is not enough optical signal in the
    //          receiver of the optical transceiver (either VTRx or SFP+).
    input          OptoLosReset_iran,

    //==== GBTx ====\\

    // I2C:
    inout          GbtxI2cSda_io,
    inout          GbtxI2cScl_io,

    // Control:
    output         GbtxReset_or,
    input          GbtxRxDataValid_i,
    output         GbtxTxDataValid_o,
    input          GbtxRxRdy_i,
    input          GbtxTxRdy_i,
    output         GbtxConfigSel_o,

    // Clocks:
    // Comment: - GbtxClockDes is the clock used by the GBTx I/O registers.
    //            GbtxClockDes is connected to the "Chip global" (Cg) clock network.
    //          - In S-GEFE, the SC Elink uses the same reference clock as the normal Elinks (GbtxClockDes).
    //            GbtxElinksScClk goes directly to the FMC connector bypassing the ProAsic3 FPGA.
    //          - GbtxElinksDclk is connected to the "Chip global" (Cg) clock network.
    input         GbtxElinksDclkCgP_ik,
    input         GbtxElinksDclkCgN_ik,

    //==== FMC Connector ====\\

    // Comment: All clocks as well as the LA, HA and DP pins are powered by Vadj,
    //          whilst the HB pins are powered by VioBM2c.

    // Clocks:
    // Comments: - FmcClkM2c0Cg    is connected to the "Chip global" (Cg) clock network.
    //             FmcClkM2c1Qg    is connected to a "Quadrant global" (Qg) clock network.
    //           - FmcClkBidir2Cq  is connected to the "Chip global" (Cg) clock network.
    //             FmcClkBidir3Qg  is connected to a "Quadrant global" (Qg) clock network.
    //           - FmcGbtClkM2c0Qg is connected to the "Chip global" (Cg) clock network.
    //             FmcGbtClkM2c1Qg is connected to a "Quadrant global" (Qg) clock network.
    input          FmcClkM2c0CgP_ik,
    input          FmcClkM2c0CgN_ik,
    input          FmcClkM2c1QgP_ik,
    input          FmcClkM2c1QgN_ik,
    inout          FmcClkBidir2CgP_iok,
    inout          FmcClkBidir2CgN_iok,
    inout          FmcClkBidir3QgP_iok,
    inout          FmcClkBidir3QgN_iok,
    input          FmcGbtClkM2c0CgP_ik,
    input          FmcGbtClkM2c0CgN_ik,
    input          FmcGbtClkM2c1QgP_ik,
    input          FmcGbtClkM2c1QgN_ik,

    // LA pins:
    // Comment: Please note that the following pins are Clock Capable (CC): 0, 1, 17.
    inout  [33: 0] FmcLaP_iob34,
    inout  [33: 0] FmcLaN_iob34,

    // HA pins:
    // Comment: Please note that the following pins are Clock Capable (CC): 0, 1, 17.
    inout  [23: 0] FmcHaP_iob24,
    inout  [23: 0] FmcHaN_iob24,

    // HB pins:
    // Comments: - Please note that the following pins are Clock Capable (CC): 0, 6, 17.
    //           - Referenced voltage levels may only be used by HB pins.
    inout  [21: 0] FmcHbP_iob22,
    inout  [21: 0] FmcHbN_iob22,

    // DP lanes:
    // Comment: The high-speed (DP) lanes do not complain the FMC standard (VITA 57.1)
    //          since they are used as standard IOs and some of them are also used for
    //          special purposes.
    inout  [ 9: 0] FmcDpM2cP_iob10,
    inout  [ 9: 0] FmcDpM2cN_iob10,
    inout  [ 9: 0] FmcDpC2mP_iob10,
    inout  [ 9: 0] FmcDpC2mN_iob10,

    // I2C:
    inout          FmcSda_io,
    inout          FmcScl_io,

    // JTAG:
    output         FmcTck_o,
    //input          FmcTdi_i,
    output         FmcTdi_i,
    output         FmcTdo_o,
    output         FmcTms_o,
    output         FmcTrstL_on,

    // Control:
    input          FmcClkDir_i,
    input          FmcPowerGoodM2c_i,
    output         FmcPowerGoodC2m_o,
    input          FmcPrsntM2cL_in,

    //==== Miscellaneous ====\\

    // Clock feedback:
    // Comment: ClkFeedbackCg_ikb2 are connected to the "Chip global" (Cg) clock network.
    input  [ 1: 0] ClkFeedbackCgP_ikb2,
    input  [ 1: 0] ClkFeedbackCgN_ikb2,
    output [ 1: 0] ClkFeedbackP_okb2,
    output [ 1: 0] ClkFeedbackN_okb2,

    // MMCX Clocks & GPIOs:
    // Comments: - MmcxClkCg is connected to the "Chip global" (Cg) clock network.
    //           - MmcxGpIo[3:0] are connected to different "Quadrant global" (Qg) clock networks.
    inout          MmcxClkIoCgP_iok,
    inout          MmcxClkIoCgN_iok,
    inout  [ 3: 0] MmcxGpIoQg_iokb4,

    // GPIO connectors:
    inout  [12: 0] GpioConnA_iob13,
    inout  [23: 0] GpioConnB_iob24,
    inout  [23: 0] GpioConnC_iob24,
    inout  [23: 0] GpioConnD_iob24,

    // Link FMC GPIOs:
    inout  [ 1: 0] LinkFmcGpIoP_iob2,
    inout  [ 1: 0] LinkFmcGpIoN_iob2,

    // Electrical serial link:
    input          ElectSerialLinkRx_i,
    output         ElectSerialLinkTx_o,

    // S-GEFE configuration ID:
    input  [ 7: 0] SGefeConfigId_ib8,

    // Push button:
    input          PushButton_i,

    // DIP switch:
    input  [ 7: 0] DipSwitch_ib8,

    // User LEDs:
    output [ 5: 0] Leds_onb6,

    //==== Powering ====\\

    input          V1p5PowerGood_i,
    input          V2p5PowerGood_i,
    input          V3p3PowerGood_i,
    output         V3p3Inhibit_o,
    input          V3p3OverCurMon_i,

    //==== System Module Interface ====\\

    // Resets scheme:
    // Comment: See Microsemi application note AC380.
    input          GeneralReset_iran,

    // GBTx:
    // Comments: - GbtxClockDesCg is the clock used by the GBTx I/O registers.
    //             GbtxClockDesCg is connected to the "Chip global" (Cg) clock network.
    input          GbtxClockDesCg_ik,
    //--
    input  [79: 0] DataFromGbtx_ib80,
    output [79: 0] DataToGbtx_ob80,
    input  [ 1: 0] DataFromGbtxSc_ib2,
    output [ 1: 0] DataToGbtxSc_ob2,

    // Crystal oscillator (25MHz):
    // Comment: - Osc25MhzCg is connected to the "Chip global" (Cg) clock network.
    input          Osc25MhzCg_ik
);

//======================================  Declarations  ======================================\\

//==== Variables ====\\

genvar         i;

//==== Wires & Regs ====\\

// Resets scheme:
wire           AppReset_ra;
// Clocks scheme
wire           GbtxElinksDclkCg_k;
wire   [ 1: 0] a_ClkFeedback_kb2;
wire   [ 1: 0] ClkFeedbackOddr_kb2;
wire   [ 1: 0] ClkFeedbackCg_kb2;
wire           ClkFeedback1_kb2;
// GBTx Elinks:
reg    [79: 0] DataGbtxElinks_qb80;
reg    [ 1: 0] DataGbtxElinksSc_qb2;
// Miscellaneous:
wire   [ 1: 0] FeedBackHb_b2;
// S-GEFE Test:
reg    [15:0] TestCntr_c16;
reg    [79:0] TestToGbtTx_qb80;
wire   [79:0] TestToGbtTx_b80;
reg    [15:0] RstTest_qb16;
wire          FmcClkM2c0;
wire          FmcClkM2c1;
wire          FmcGbtClkM2c0;
wire          FmcGbtClkM2c1;
wire          MmcxClkIo;
wire          TriggerFmcGpIOBusTest;
wire   [99:0] FmcIoDataI_b100;
wire   [99:0] FmcIoDataO_b100;
wire          FmcGpIOBusCorrect;

//=======================================  User Logic  =======================================\\

//==== Resets Scheme ====\\

assign AppReset_ra = ~GeneralReset_iran;

//==== Clocks Scheme ====\\

// GBTx Elinks EXTRA clock:
// Comment: Note!! In S-GEFE, GbtxElinksDclkCg is NOT used for clocking the Elinks (GbtxClockDesCg is used instead).
CLKBUF_LVDS i_GbtxElinksDclkCg_ibuf (
    .PADP (GbtxElinksDclkCgP_ik),
    .PADN (GbtxElinksDclkCgN_ik),
    .Y    (GbtxElinksDclkCg_k));

// Feedback Clocks:
assign a_ClkFeedback_kb2 = {GbtxElinksDclkCg_k, Osc25MhzCg_ik};

generate for (i=0; i<2; i=i+1) begin: ClkFeedback_gen
    DDR_OUT i_ClkFeedback_oddr (
       .DR   (1'b1),
       .DF   (1'b0),
       .CLK  (a_ClkFeedback_kb2  [i]),
       .CLR  (1'b0),
       .Q    (ClkFeedbackOddr_kb2[i]));
    //--
    OUTBUF_LVDS i_ClkFeedback_obuf (
       .D    (ClkFeedbackOddr_kb2[i]),
       .PADP (ClkFeedbackP_okb2  [i]),
       .PADN (ClkFeedbackN_okb2  [i]));
end endgenerate

CLKBUF_LVDS i_ClkFeedback0_ibuf (
    .PADP (ClkFeedbackCgP_ikb2[0]),
    .PADN (ClkFeedbackCgN_ikb2[0]),
    .Y    (ClkFeedbackCg_kb2  [0]));

// Comment: Note!! ClkFeedback1 enters the FPGA through a standard user I/O and then is forwarded to the "Chip Global" (Cg)
//                 clock network throgh an internal clock buffer (CLKINT).
//                 The reason of this is because the clock buffer of the I/O pin is already used by Osc25MhzCg.
INBUF_LVDS i_ClkFeedback1_ibuf (
    .PADP (ClkFeedbackCgP_ikb2[1]),
    .PADN (ClkFeedbackCgN_ikb2[1]),
    .Y    (ClkFeedback1_kb2));

CLKINT i_ClkFeedback1_Chbuf (
    .Y (ClkFeedbackCg_kb2[1]),
    .A (ClkFeedback1_kb2));

//==== GBTx ====\\

// Elinks:
// Comment: GbtxClockDesCg_ik is the clock used by the GBTx I/O registers.
always @(posedge GbtxClockDesCg_ik) begin
    DataGbtxElinks_qb80  <= DataFromGbtx_ib80;
    DataGbtxElinksSc_qb2 <= DataFromGbtxSc_ib2;
end

assign DataToGbtx_ob80  = DipSwitch_ib8[7] ? TestToGbtTx_qb80 : DataGbtxElinks_qb80;
assign DataToGbtxSc_ob2 = DataGbtxElinksSc_qb2;

// GBTx Control:
assign GbtxTxDataValid_o =  DipSwitch_ib8[1];
assign Leds_onb6[5]      = ~GbtxRxDataValid_i;

//==== Miscellaneous ====\\

// Heartbeats:
HeartBeat #(
    .g_ClkFrequency_b32 (32'd25000000))
i_Osc25MhzFeedBack0Hb (
    .Reset_ira          (AppReset_ra),
    .Clk_ik             (ClkFeedbackCg_kb2[0]),
    .HeartBeat_oq       (FeedBackHb_b2[0]));

HeartBeat #(
    .g_ClkFrequency_b32 (32'd40000000))
i_GbtxElinksDclkFeedBack1Hb (
    .Reset_ira          (AppReset_ra),
    .Clk_ik             (ClkFeedbackCg_kb2[1]),
    .HeartBeat_oq       (FeedBackHb_b2[1]));

assign Leds_onb6[2] = DipSwitch_ib8[0] ? FeedBackHb_b2[1] : FeedBackHb_b2[0];

HeartBeat #(
    .g_ClkFrequency_b32 (32'd40000000))
i_DclkHb (
    .Reset_ira          (AppReset_ra),
    .Clk_ik             (GbtxClockDesCg_ik),
    .HeartBeat_oq       (Leds_onb6[3]));

// Opto Loss Of Signal (LOS) reset:
// Comment: This reset is asserted when there is not enough optical signal in the
//          receiver of the optical transceiver (either VTRx or SFP+).
//
// Comment: Added GbtxRxRdy and GbtxTxRdy for S-GEFE test.
assign Leds_onb6[4] = ~(OptoLosReset_iran && GbtxRxRdy_i && GbtxTxRdy_i);

// Push button:
assign Leds_onb6[1] = ~PushButton_i;

//==== Powering ====\\

assign Leds_onb6[0] = ~(V1p5PowerGood_i && V2p5PowerGood_i && V3p3PowerGood_i && ~V3p3OverCurMon_i);

//==== S-GEFE Test ====\\

// Comment: Some features are already tested by the logic of the base projec.

// Test counter:
always @(posedge Osc25MhzCg_ik) TestCntr_c16 <= AppReset_ra ? 16'h0 : TestCntr_c16 + 1;

// Test signals to GBT TX:
assign TestToGbtTx_b80[79:33] = 47'h0;
assign TestToGbtTx_b80[   32] = FmcGpIOBusCorrect;
assign TestToGbtTx_b80[31:16] = RstTest_qb16;
assign TestToGbtTx_b80[15:11] = 5'h0;
assign TestToGbtTx_b80[10: 8] = {FmcClkDir_i,FmcPowerGoodM2c_i,FmcPrsntM2cL_in};
assign TestToGbtTx_b80[ 7: 0] = SGefeConfigId_ib8;

always @(posedge GbtxClockDesCg_ik) TestToGbtTx_qb80 <= TestToGbtTx_b80;

// Power-Up Reset:
always @(posedge GbtxClockDesCg_ik or posedge AppReset_ra) RstTest_qb16 <= AppReset_ra ? 16'hCAFE : (DipSwitch_ib8[7] ? RstTest_qb16 : 16'h0);

// Clocks Scheme:
OUTBUF_LVDS i_FmcClkBidir2_obuf (
    .D    (Osc25MhzCg_ik),
    .PADP (FmcClkBidir2CgP_iok),
    .PADN (FmcClkBidir2CgN_iok));

INBUF_LVDS iFmcClkM2c0_ibuf (
    .PADP (FmcClkM2c0CgP_ik),
    .PADN (FmcClkM2c0CgN_ik),
    .Y    (FmcClkM2c0));

OUTBUF_LVDS i_FmcClkBidir3_obuf (
    .D    (GbtxElinksDclkCg_k),
    .PADP (FmcClkBidir3QgP_iok),
    .PADN (FmcClkBidir3QgN_iok));

INBUF_LVDS i_FmcClkM2c1_ibuf (
    .PADP (FmcClkM2c1QgP_ik),
    .PADN (FmcClkM2c1QgN_ik),
    .Y    (FmcClkM2c1));

INBUF_LVDS i_FmcGbtClkM2c0_ibuf (
    .PADP (FmcGbtClkM2c0CgP_ik),
    .PADN (FmcGbtClkM2c0CgN_ik),
    .Y    (FmcGbtClkM2c0));

INBUF_LVDS i_FmcGbtClkM2c1_ibuf (
    .PADP (FmcGbtClkM2c1QgP_ik),
    .PADN (FmcGbtClkM2c1QgN_ik),
    .Y    (FmcGbtClkM2c1));

INBUF_LVDS i_MmcxClkIo_ibuf (
    .PADP (MmcxClkIoCgP_iok),
    .PADN (MmcxClkIoCgN_iok),
    .Y    (MmcxClkIo));

// GBTx Control:
assign GbtxI2cSda_io   = (DipSwitch_ib8[7] && DipSwitch_ib8[0]) ? 1'b0 : 1'bZ;
assign GbtxI2cScl_io   = (DipSwitch_ib8[7] && DipSwitch_ib8[1]) ? 1'b0 : 1'bZ;
assign GbtxConfigSel_o = (DipSwitch_ib8[7] && DipSwitch_ib8[2]) ? 1'b0 : 1'bZ;
assign GbtxReset_or    = (DipSwitch_ib8[7] && DipSwitch_ib8[3]) ? 1'b0 : 1'b1;

// FMC GPIOs & DP Lines:
UserIoChecker #(
    .g_BusWidth  (100),
    .g_ReadDly   (30),
    .g_TestDly   (10))
i_FmcUserIoChecker (
   .Reset_ir     (AppReset_ra || (DipSwitch_ib8[7] && DataGbtxElinks_qb80[0])),
   .Clk_ik       (GbtxClockDesCg_ik),
   .Trigger_i    (TriggerFmcGpIOBusTest),
   .Data_ib      (FmcIoDataI_b100),
   .Data_ob      (FmcIoDataO_b100),
   .Busy_o       (),
   .BusErrors_ob (),
   .BusCorrect_o (FmcGpIOBusCorrect));

// Comment: See TestToGbtTx assigment.

assign TriggerFmcGpIOBusTest = DipSwitch_ib8[7] && (DataFromGbtx_ib80[1] && ~DataGbtxElinks_qb80[1]);

generate for (i=0;i<17;i=i+1) begin: Gen_FmcLa
    assign FmcLaP_iob34   [2*i   ] = FmcIoDataO_b100[   i   ];
    assign FmcLaN_iob34   [2*i   ] = FmcIoDataO_b100[   i+17];
    assign FmcIoDataI_b100[  i   ] = FmcLaP_iob34   [(2*i)+1];
    assign FmcIoDataI_b100[  i+17] = FmcLaN_iob34   [(2*i)+1];
end endgenerate

generate for (i=0;i<12;i=i+1) begin: Gen_FmcHa
    assign FmcHaP_iob24   [2*i   ] = FmcIoDataO_b100[   i+34];
    assign FmcHaN_iob24   [2*i   ] = FmcIoDataO_b100[   i+46];
    assign FmcIoDataI_b100[  i+34] = FmcHaP_iob24   [(2*i)+1];
    assign FmcIoDataI_b100[  i+46] = FmcHaN_iob24   [(2*i)+1];
end endgenerate

generate for (i=0;i<11;i=i+1) begin: Gen_FmcHa
    assign FmcHbP_iob22   [2*i   ] = FmcIoDataO_b100[   i+58];
    assign FmcHbN_iob22   [2*i   ] = FmcIoDataO_b100[   i+69];
    assign FmcIoDataI_b100[  i+58] = FmcHbP_iob22   [(2*i)+1];
    assign FmcIoDataI_b100[  i+69] = FmcHbN_iob22   [(2*i)+1];
end endgenerate

generate for (i=0;i<10;i=i+1) begin: Gen_FmcHa
    assign FmcDpC2mP_iob10[i   ] = FmcIoDataO_b100[i+80];
    assign FmcDpC2mN_iob10[i   ] = FmcIoDataO_b100[i+90];
    assign FmcIoDataI_b100[i+80] = FmcDpM2cP_iob10[i   ];
    assign FmcIoDataI_b100[i+90] = FmcDpM2cN_iob10[i   ];
end endgenerate

// FMC Control:
assign FmcSda_io   = TestCntr_c16[6];
assign FmcScl_io   = TestCntr_c16[5];
assign FmcTck_o    = TestCntr_c16[4];
assign FmcTdi_i    = TestCntr_c16[3];
assign FmcTdo_o    = TestCntr_c16[2];
assign FmcTms_o    = TestCntr_c16[1];
assign FmcTrstL_on = TestCntr_c16[0];

// Comment: See TestToGbtTx assigment.

HeartBeat #(
    .g_ClkFrequency_b32 (32'd40000000))
i_FmcPowerGoodC2mHb (
    .Reset_ira          (AppReset_ra),
    .Clk_ik             (GbtxClockDesCg_ik),
    .HeartBeat_oq       (FmcPowerGoodC2m_o));

// GPIOs:
assign GpioConnA_iob13 =  TestCntr_c16[12:0];
assign GpioConnC_iob24 = {TestCntr_c16[11:0],TestCntr_c16[11:0]};
assign GpioConnB_iob24 = {TestCntr_c16[11:0],TestCntr_c16[11:0]};
assign GpioConnD_iob24 = {TestCntr_c16[11:0],TestCntr_c16[11:0]};

// MMCX GPIOs:
assign MmcxGpIoQg_iokb4[0] = DipSwitch_ib8[2] ? AppReset_ra         : GbtxElinksDclkCg_k;
assign MmcxGpIoQg_iokb4[1] = DipSwitch_ib8[3] ? FmcClkM2c1          : FmcClkM2c0;
assign MmcxGpIoQg_iokb4[2] = DipSwitch_ib8[4] ? FmcGbtClkM2c1       : FmcGbtClkM2c0;
assign MmcxGpIoQg_iokb4[3] = DipSwitch_ib8[5] ? ElectSerialLinkRx_i : MmcxClkIo;

// Link FMC GPIOs:
assign LinkFmcGpIoP_iob2 = {TestCntr_c16[2],TestCntr_c16[0]};
assign LinkFmcGpIoN_iob2 = {TestCntr_c16[3],TestCntr_c16[1]};

// Link FMC GPIOs:
assign ElectSerialLinkTx_o = TestCntr_c16[3];

// S-GEFE configuration ID:
// Comment: See TestToGbtTx assigment.

//==== Powering Control ====\\

assign V3p3Inhibit_o = DipSwitch_ib8[6];

endmodule
