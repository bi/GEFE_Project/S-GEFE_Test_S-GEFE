//============================================================================================\\
//##################################   Module Information   ##################################\\
//============================================================================================\\
//
// Company: CERN (BE-BI)
//
// File Name: SGefeTop.sv
//
// File versions history:
//
//       DATE          VERSION      AUTHOR             DESCRIPTION
//     - 16/03/2018    2.1          M. Barros Marin    Renamed differential ports
//     - 13/02/2018    2.0          M. Barros Marin    Updated to S-GEFE
//     - 28/11/2016    1.6          M. Barros Marin    Clocks scheme modification
//     - 03/03/2016    1.5          M. Barros Marin    Cosmetic modifications
//     - 20/10/2015    1.0          M. Barros Marin    First .v module definition
//
// Language: SystemVerilog 2012
//
// Targeted device:
//
//     - Vendor: Microsemi
//     - Model:  ProASIC3E(A3PE3000)/ProASIC3L(A3PE3000L) - 896 FBGA
//
// Description:
//
//     Generic HDL "top" module for the Split - GBT-based Expandable Front-End (S-GEFE)
//     (L-GEFE + C-GEFE), the standard rad-hard digital board for CERN BE-BI applications.
//
// !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
// !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! IMPORTANT !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
// !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
// !!                                                                                      !!
// !!  It is recommended to do not modify the HDL code placed within this "top" module.    !!
// !!                                                                                      !!
// !! The user's HDL code should be placed within the "user" module (SGefeApplication.sv). !!
// !!                                                                                      !!
// !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
// !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
//
//============================================================================================\\
//############################################################################################\\
//============================================================================================\\

`timescale 1ns/100ps

module SGefeTop
//========================================  I/O ports  =======================================\\
(
    //==== Resets Scheme ====\\

    // General Reset:
    // Comment: See Microsemi application note AC380.
    inout          FpgaReset_ira,

    // Opto Loss Of Signal (LOS) reset:
    // Comment: This reset is asserted when there is not enough optical signal in the
    //          receiver of the optical transceiver (either VTRx or SFP+).
    input          OptoLosReset_iran,

    //==== GBTx ====\\

    // Clocks:
    // Comment: - GbtxClockDes is the clock used by the GBTx I/O registers.
    //            GbtxClockDes is connected to the "Chip global" (Cg) clock network.
    //            GbtxClockDes[1] from L-GEFE goes directly to the FMC connector bypassing the ProAsic3 FPGA.
    //          - In S-GEFE, the SC Elink uses the same reference clock as the normal Elinks (GbtxClockDes).
    //            GbtxElinksScClk goes directly to the FMC connector bypassing the ProAsic3 FPGA.
    //          - GbtxElinksDclk is connected to the "Chip global" (Cg) clock network.
    input         GbtxClockDesP_ik,
    input         GbtxClockDesN_ik,
    input         GbtxElinksDclkP_ik,
    input         GbtxElinksDclkN_ik,

    // Elinks:
    // Comment: In S-GEFE, the GbtxElinksDio pins are only used as INPUTs.
    input  [15: 0] GbtxElinksDioP_ib16,
    input  [15: 0] GbtxElinksDioN_ib16,
    input  [39:16] GbtxElinksDoutP_ib24,
    input  [39:16] GbtxElinksDoutN_ib24,
    output [39: 0] GbtxElinksDinP_ob40,
    output [39: 0] GbtxElinksDinN_ob40,

    // Slow Control (SC) Elink:
    input          GbtxElinksScOutP_i,
    input          GbtxElinksScOutN_i,
    output         GbtxElinksScInP_o,
    output         GbtxElinksScInN_o,

    // I2C:
    inout          GbtxI2cSda_io,
    inout          GbtxI2cScl_io,

    // Control:
    output         GbtxReset_or,
    input          GbtxRxDataValid_i,
    output         GbtxTxDataValid_o,
    input          GbtxRxRdy_i,
    input          GbtxTxRdy_i,
    output         GbtxConfigSel_o,
//    input          GbtxConfigSelV2p5_BUG_i, // Comment: Note!! This signal is a bug in the design and must be set as input or high impedance 'Z'

    //==== FMC connector ====\\

    // Clocks:
    input  [ 1: 0] FmcClkM2cP_ikb2,
    input  [ 1: 0] FmcClkM2cN_ikb2,
    inout  [ 3: 2] FmcClkBidirP_iokb2,
    inout  [ 3: 2] FmcClkBidirN_iokb2,
    input  [ 1: 0] FmcGbtClkM2cP_ikb2,
    input  [ 1: 0] FmcGbtClkM2cN_ikb2,

    // Comment: The LA, HA and DP pins are powered by Vadj, whilst the HB pins are powered
    //          by VioBM2c.

    // LA pins:
    // Comment: Please note that the following pins are Clock Capable (CC): 0, 1, 17, 18.
    inout  [33: 0] FmcLaP_iob34,
    inout  [33: 0] FmcLaN_iob34,

    // HA pins:
    // Comment: Please note that the following pins are Clock Capable (CC): 0, 1, 17, 18.
    inout  [23: 0] FmcHaP_iob24,
    inout  [23: 0] FmcHaN_iob24,

    // HB pins:
    // Comments: - Please note that the following pins are Clock Capable (CC): 0, 6, 17.
    //           - Referenced voltage levels may only be used by HB pins.
    inout  [21: 0] FmcHbP_iob22,
    inout  [21: 0] FmcHbN_iob22,

    // DP lanes:
    // Comment: The high-speed (DP) lanes do not complain the FMC standard (VITA 57.1)
    //          since they are used as standard IOs and some of them are also used for
    //          special purposes.
    inout  [ 9: 0] FmcDpM2cP_iob10,
    inout  [ 9: 0] FmcDpM2cN_iob10,
    inout  [ 9: 0] FmcDpC2mP_iob10,
    inout  [ 9: 0] FmcDpC2mN_iob10,

    //==== I2C ====\\

    inout          FmcSda_io,
    inout          FmcScl_io,

    //==== JTAG ====\\

    output         FmcTck_o,
//    input          FmcTdi_i,
    output         FmcTdi_i,
    output         FmcTdo_o,
    output         FmcTms_o,
    output         FmcTrstL_on,

    //==== Control ====\\

    input          FmcClkDir_i,
    input          FmcPowerGoodM2c_i,
    output         FmcPowerGoodC2m_o,
    input          FmcPrsntM2cL_in,

    //==== Miscellaneous ====\\

    // Crystal oscillator (25MHz):
    // Comment: Osc25Mhz is connected to the "Chip global" (Cg) clock network.
    input          Osc25Mhz_ik,

    // Clock feedback:
    // Comment: ClkFeedbackI is connected to the "Chip global" (Cg) clock network.
    input  [ 1: 0] ClkFeedbackP_ikb2,
    input  [ 1: 0] ClkFeedbackN_ikb2,
    output [ 1: 0] ClkFeedbackP_okb2,
    output [ 1: 0] ClkFeedbackN_okb2,

    // MMCX Clocks & GPIOs:
    // Comment: - MmcxClkIo is connected to the "Chip global" (Cg) clock network.
    //          - MmcxGpIo[3:0] are connected to different "Quadrant global" (Qg) clock networks.
    inout          MmcxClkIoP_iok,
    inout          MmcxClkIoN_iok,
    inout  [ 3: 0] MmcxGpIo_iokb4,

    // GPIO connectors:
    inout  [12: 0] GpioConnA_iob13,
    inout  [23: 0] GpioConnB_iob24,
    inout  [23: 0] GpioConnC_iob24,
    inout  [23: 0] GpioConnD_iob24,

    // Link FMC GPIOs:
    inout          LinkFmcGpIoAP_io,
    inout          LinkFmcGpIoAN_io,
    inout          LinkFmcGpIoBP_io,
    inout          LinkFmcGpIoBN_io,

    // Electrical serial link:
    input          ElectSerialLinkRx_i,
    output         ElectSerialLinkTx_o,

    // S-GEFE configuration ID:
    input  [ 7: 0] SGefeConfigId_ib8,

    // Push button:
    input          PushButton_i,

    // DIP switch:
    input  [ 7: 0] DipSwitch_ib8,

    // User LEDs:
    output [ 5: 0] Leds_onb6,

    //==== Powering ====\\

    input          V1p5PowerGood_i,
    input          V2p5PowerGood_i,
    input          V3p3PowerGood_i,
    output         V3p3Inhibit_o,
    input          V3p3OverCurMon_i
);

//======================================  Declarations  ======================================\\

//==== Wires & Regs ====\\

// Resets scheme:
wire         GeneralReset_ran;

// GBTx:
wire         GbtxClockDesCg_k;
wire [79: 0] DataFromGbtx_b80;
wire [79: 0] DataToGbtx_b80;
wire [ 1: 0] DataFromGbtxSc_b2;
wire [ 1: 0] DataToGbtxSc_b2;

// Miscellaneous:
wire         Osc25MhzCg_k;

//=======================================  User Logic  =======================================\\

// System module:
SGefeSystem i_SGefeSystem (
    // Resets scheme:
    .FpgaReset_ira           (FpgaReset_ira),
    // GBTx:
    .GbtxClockDesCgP_ik      (GbtxClockDesP_ik),
    .GbtxClockDesCgN_ik      (GbtxClockDesN_ik),
    //--
    .GbtxElinksDioP_ib16     (GbtxElinksDioP_ib16), // Comment: In S-GEFE, the GbtxElinksDio pins are only used as INPUTs.
    .GbtxElinksDioN_ib16     (GbtxElinksDioN_ib16), //
    .GbtxElinksDoutP_ib24    (GbtxElinksDoutP_ib24),
    .GbtxElinksDoutN_ib24    (GbtxElinksDoutN_ib24),
    .GbtxElinksDinP_ob40     (GbtxElinksDinP_ob40),
    .GbtxElinksDinN_ob40     (GbtxElinksDinN_ob40),
    //--
    .GbtxElinksScOutP_i      (GbtxElinksScOutP_i),
    .GbtxElinksScOutN_i      (GbtxElinksScOutN_i),
    .GbtxElinksScInP_o       (GbtxElinksScInP_o),
    .GbtxElinksScInN_o       (GbtxElinksScInN_o),
    //--
//    .GbtxConfigSelV2p5_BUG_i (GbtxConfigSelV2p5_BUG_i), // Comment: Note!! This signal is a bug in the design and must be set as input or high impedance 'Z'
    // Miscellaneous:
    .Osc25Mhz_ik             (Osc25Mhz_ik),
    // User module interface:
    .GeneralReset_oran       (GeneralReset_ran),
    //--
    .GbtxClockDesCg_ok       (GbtxClockDesCg_k),
    //--
    .DataFromGbtx_ob80       (DataFromGbtx_b80),
    .DataToGbtx_ib80         (DataToGbtx_b80),
    .DataFromGbtxSc_ob2      (DataFromGbtxSc_b2),
    .DataToGbtxSc_ib2        (DataToGbtxSc_b2),
    //--
    .Osc25MhzCg_ok           (Osc25MhzCg_k));

// Application Module:
SGefeApplication i_SGefeApplication (
    // Resets scheme:
    .OptoLosReset_iran    (OptoLosReset_iran),
    // GBTx:
    .GbtxI2cSda_io        (GbtxI2cSda_io),
    .GbtxI2cScl_io        (GbtxI2cScl_io),
    //--
    .GbtxReset_or         (GbtxReset_or),
    .GbtxRxDataValid_i    (GbtxRxDataValid_i),
    .GbtxTxDataValid_o    (GbtxTxDataValid_o),
    .GbtxRxRdy_i          (GbtxRxRdy_i),
    .GbtxTxRdy_i          (GbtxTxRdy_i),
    .GbtxConfigSel_o      (GbtxConfigSel_o),
    //--
    .GbtxElinksDclkCgP_ik (GbtxElinksDclkP_ik),
    .GbtxElinksDclkCgN_ik (GbtxElinksDclkN_ik),
    // FMC connector:
    .FmcClkM2c0CgP_ik     (FmcClkM2cP_ikb2 [0]),
    .FmcClkM2c0CgN_ik     (FmcClkM2cN_ikb2 [0]),
    .FmcClkM2c1QgP_ik     (FmcClkM2cP_ikb2 [1]),
    .FmcClkM2c1QgN_ik     (FmcClkM2cN_ikb2 [1]),
    .FmcClkBidir2CgP_iok  (FmcClkBidirP_iokb2[2]),
    .FmcClkBidir2CgN_iok  (FmcClkBidirN_iokb2[2]),
    .FmcClkBidir3QgP_iok  (FmcClkBidirP_iokb2[3]),
    .FmcClkBidir3QgN_iok  (FmcClkBidirN_iokb2[3]),
    .FmcGbtClkM2c0CgP_ik  (FmcGbtClkM2cP_ikb2[0]),
    .FmcGbtClkM2c0CgN_ik  (FmcGbtClkM2cN_ikb2[0]),
    .FmcGbtClkM2c1QgP_ik  (FmcGbtClkM2cP_ikb2[1]),
    .FmcGbtClkM2c1QgN_ik  (FmcGbtClkM2cN_ikb2[1]),
    //--
    .FmcLaP_iob34         (FmcLaP_iob34),
    .FmcLaN_iob34         (FmcLaN_iob34),
    //--
    .FmcHaP_iob24         (FmcHaP_iob24),
    .FmcHaN_iob24         (FmcHaN_iob24),
    //--
    .FmcHbP_iob22         (FmcHbP_iob22),
    .FmcHbN_iob22         (FmcHbN_iob22),
    //--
    .FmcDpM2cP_iob10      (FmcDpM2cP_iob10),
    .FmcDpM2cN_iob10      (FmcDpM2cN_iob10),
    .FmcDpC2mP_iob10      (FmcDpC2mP_iob10),
    .FmcDpC2mN_iob10      (FmcDpC2mN_iob10),
    //--
    .FmcSda_io            (FmcSda_io),
    .FmcScl_io            (FmcScl_io),
    //--
    .FmcTck_o             (FmcTck_o),
    .FmcTdi_i             (FmcTdi_i),
    .FmcTdo_o             (FmcTdo_o),
    .FmcTms_o             (FmcTms_o),
    .FmcTrstL_on          (FmcTrstL_on),
    //--
    .FmcClkDir_i          (FmcClkDir_i),
    .FmcPowerGoodM2c_i    (FmcPowerGoodM2c_i),
    .FmcPowerGoodC2m_o    (FmcPowerGoodC2m_o),
    .FmcPrsntM2cL_in      (FmcPrsntM2cL_in),
    // Miscellaneous:
    .ClkFeedbackCgP_ikb2  (ClkFeedbackP_ikb2),
    .ClkFeedbackCgN_ikb2  (ClkFeedbackN_ikb2),
    .ClkFeedbackP_okb2    (ClkFeedbackP_okb2),
    .ClkFeedbackN_okb2    (ClkFeedbackN_okb2),
    //--
    .MmcxClkIoCgP_iok     (MmcxClkIoP_iok),
    .MmcxClkIoCgN_iok     (MmcxClkIoN_iok),
    .MmcxGpIoQg_iokb4     (MmcxGpIo_iokb4),
    //--
    .GpioConnA_iob13      (GpioConnA_iob13),
    .GpioConnB_iob24      (GpioConnB_iob24),
    .GpioConnC_iob24      (GpioConnC_iob24),
    .GpioConnD_iob24      (GpioConnD_iob24),
    //--
    .LinkFmcGpIoP_iob2    ({LinkFmcGpIoBP_io, LinkFmcGpIoAP_io}),
    .LinkFmcGpIoN_iob2    ({LinkFmcGpIoBN_io, LinkFmcGpIoAN_io}),
    //--
    .ElectSerialLinkRx_i  (ElectSerialLinkRx_i),
    .ElectSerialLinkTx_o  (ElectSerialLinkTx_o),
    //--
    .SGefeConfigId_ib8    (SGefeConfigId_ib8),
    //--
    .PushButton_i         (PushButton_i),
    //--
    .DipSwitch_ib8        (DipSwitch_ib8),
    //--
    .Leds_onb6            (Leds_onb6),
    // Powering:
    .V1p5PowerGood_i      (V1p5PowerGood_i),
    .V2p5PowerGood_i      (V2p5PowerGood_i),
    .V3p3PowerGood_i      (V3p3PowerGood_i),
    .V3p3Inhibit_o        (V3p3Inhibit_o),
    .V3p3OverCurMon_i     (V3p3OverCurMon_i),
    // System module interface:
    .GeneralReset_iran    (GeneralReset_ran),
    //--
    .GbtxClockDesCg_ik    (GbtxClockDesCg_k),
    //--
    .DataFromGbtx_ib80    (DataFromGbtx_b80),
    .DataToGbtx_ob80      (DataToGbtx_b80),
    .DataFromGbtxSc_ib2   (DataFromGbtxSc_b2),
    .DataToGbtxSc_ob2     (DataToGbtxSc_b2),
    //--
    .Osc25MhzCg_ik        (Osc25MhzCg_k));

endmodule
