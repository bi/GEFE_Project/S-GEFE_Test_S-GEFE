`timescale 1ns/100ps

//Manoel Barros Marin, BE-BI-QP (CERN) - 04/06/15

module UserIoChecker # (

//***********   
//Parameters:
//***********

    parameter        g_BusWidth = 80,    
                     g_ReadDly  = 30,   //10cycles@125MHz = 240ns
                     g_TestDly  = 10) (

//*****   
//I/Os:
//***** 
   
   input                       Reset_ir,        
   input                       Clk_ik,      
   input                       Trigger_i,   
   input      [g_BusWidth-1:0] Data_ib,     
   output     [g_BusWidth-1:0] Data_ob,     
   output reg                  Busy_o,      
   output     [g_BusWidth-1:0] BusErrors_ob,
   output reg                  BusCorrect_o

);

//*************   
//Declarations:
//*************  

   localparam s_Idle              = 6'b000001,
              s_SendMarchingZero  = 6'b000010,
              s_CheckMarchingZero = 6'b000100,
              s_SendMarchingOne   = 6'b001000,
              s_CheckMarchingOne  = 6'b010000,
              s_TestDone          = 6'b100000;
   
   reg [           5:0] State_qb6, NextState_qb6;
   reg [           7:0] Timer_qb8;     
   reg [g_BusWidth-1:0] DataO_qb;      
   reg [g_BusWidth-1:0] DataI_qb;      
   reg [g_BusWidth-1:0] ZerosErrors_qb;
   reg [g_BusWidth-1:0] OnesErrors_qb;
   reg [g_BusWidth-1:0] BusErrors_qb;
   
   integer i;
   
//***********      
//User Logic:
//***********  

    //FSM:
    always @(posedge Clk_ik) State_qb6 <= #1 Reset_ir ? s_Idle : NextState_qb6;
    always @* begin
        NextState_qb6             = State_qb6;
        case(State_qb6)
            s_Idle: begin
                if (Trigger_i) begin
                    NextState_qb6 = s_SendMarchingZero;
                end
            end         
            s_SendMarchingZero: begin
                if (Timer_qb8 == g_ReadDly-1) begin
                    NextState_qb6 = s_CheckMarchingZero;
                end
            end      
            s_CheckMarchingZero: begin
                if (i == g_BusWidth-1) begin
                    NextState_qb6 = s_SendMarchingOne;
                end else begin
                    NextState_qb6 = s_SendMarchingZero;
                end
            end
            s_SendMarchingOne: begin
                if (Timer_qb8 == g_ReadDly-1) begin
                    NextState_qb6 = s_CheckMarchingOne;
                end
            end      
            s_CheckMarchingOne: begin
                if (i == g_BusWidth) begin
                    NextState_qb6 = s_TestDone;
                end else begin
                    NextState_qb6 = s_SendMarchingOne;
                end
            end         
            s_TestDone: begin
            end
            default: begin
                NextState_qb6     = s_Idle;
            end
        endcase
    end
    always @(posedge Clk_ik)
        if (Reset_ir) begin    
            i                                 <= #1 0;
            Busy_o                            <= #1 1'b0;
            Timer_qb8                         <= #1 8'b0;
            DataO_qb                          <= #1 {g_BusWidth{1'b1}};
            ZerosErrors_qb                    <= #1 {g_BusWidth{1'b0}};
            OnesErrors_qb                     <= #1 {g_BusWidth{1'b0}};
            BusErrors_qb                      <= #1 {g_BusWidth{1'b0}};
            BusCorrect_o                      <= #1 1'b0;
        end else begin
            case(State_qb6) 
                s_Idle: begin
                    i                     <= #1 0;
                    Busy_o                <= #1 1'b0;
                    Timer_qb8             <= #1 8'b0;
                    DataO_qb              <= #1 {g_BusWidth{1'b1}};
                    ZerosErrors_qb        <= #1 {g_BusWidth{1'b0}};
                    OnesErrors_qb         <= #1 {g_BusWidth{1'b0}};
                    BusErrors_qb          <= #1 {g_BusWidth{1'b0}};
                    BusCorrect_o          <= #1 1'b0;
                end   
                s_SendMarchingZero: begin
                    Busy_o                <= #1 1'b1;
                    if (i > 0) begin
                        DataO_qb[i-1]     <= #1 1'b1;
                    end
                    DataO_qb[i]           <= #1 1'b0;
                    Timer_qb8             <= #1 Timer_qb8+1; 
                end
                s_CheckMarchingZero: begin
                    Timer_qb8             <= #1 8'b0;
                    if (DataI_qb[i] != DataO_qb[i]) begin
                        ZerosErrors_qb[i] <= #1 'b1;
                    end
                    if (NextState_qb6 == s_SendMarchingOne) begin
                        i                 <= #1 1'b0;
                        DataO_qb          <= #1 {g_BusWidth{1'b0}};
                    end else begin
                        i                 <= #1 i+1;
                    end
                end
                s_SendMarchingOne: begin
                    if (i > 0) begin
                        DataO_qb[i-1]     <= #1 1'b0;
                    end
                    DataO_qb[i]           <= #1 1'b1;
                    Timer_qb8             <= #1 Timer_qb8+1; 
                end
                s_CheckMarchingOne: begin
                    Timer_qb8             <= #1 8'b0;
                    if (DataI_qb[i] != DataO_qb[i]) begin
                        OnesErrors_qb[i]  <= #1 'b1;
                    end
                    if (NextState_qb6 == s_TestDone) begin
                        i                 <= #1 1'b0;
                    end else begin
                        i                 <= #1 i+1;
                    end
                end
                s_TestDone: begin
                    if (Timer_qb8 == g_TestDly-1) begin
                        Busy_o            <= #1 1'b0;
                        BusErrors_qb      <= #1    ZerosErrors_qb | OnesErrors_qb;
                        BusCorrect_o      <= #1 ~|(ZerosErrors_qb | OnesErrors_qb);
                    end else begin
                       Timer_qb8          <= #1 Timer_qb8+1; 
                    end
                end
            endcase
        end

    assign Data_ob      = DataO_qb;
    assign BusErrors_ob = BusErrors_qb;
    
    //Sequential Logic:
    always @(posedge Clk_ik)
        if (Reset_ir) begin  
            DataI_qb <= #1 {g_BusWidth{1'b0}};
        end else begin         
            DataI_qb <= #1 Data_ib;
        end
    
endmodule