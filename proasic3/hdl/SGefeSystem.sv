//============================================================================================\\
//##################################   Module Information   ##################################\\
//============================================================================================\\
//
// Company: CERN (BE-BI)
//
// File Name: SGefeSystem.v
//
// File versions history:
//
//       DATE          VERSION      AUTHOR             DESCRIPTION
//     - 16/03/2018    2.1          M. Barros Marin    Renamed differential ports
//     - 13/02/2018    2.0          M. Barros Marin    Updated to S-GEFE
//     - 24/11/2016    1.7          M. Barros Marin    - Removed unused Elinks Clocks and moved to Application
//                                                     - Removed TX registers for compensating DDR delay
//     - 09/03/2016    1.6          M. Barros Marin    Added #1 delay for simulation
//     - 03/03/2016    1.5          M. Barros Marin    Replaced DDR_IO by DDR_I & cosmetic modifications
//     - 23/10/2015    1.0          M. Barros Marin    First .v module definition
//
// Language: SystemVerilog 2012
//
// Targeted device:
//
//     - Vendor: Microsemi
//     - Model:  ProASIC3E(A3PE3000)/ProASIC3L(A3PE3000L) - 896 FBGA
//
// Description:
//
//     Generic HDL "system" module for the Split - GBT-based Expandable Front-End (S-GEFE)
//     (L-GEFE + C-GEFE), the standard rad-hard digital board for CERN BE-BI applications.
//
// !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
// !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! IMPORTANT !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
// !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
// !!                                                                                        !!
// !!  It is recommended to do not modify the HDL code placed within this "system" module.   !!
// !!                                                                                        !!
// !! The user's HDL code should be placed within the "user" module (SGefeApplication.sv).   !!
// !!                                                                                        !!
// !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
// !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
//
//============================================================================================\\
//############################################################################################\\
//============================================================================================\\

`timescale 1ns/100ps

module SGefeSystem
//========================================  I/O ports  =======================================\\
(
    //==== Resets Scheme ====\\

    // General reset:
    // Comment: See Microsemi application note AC380.
    inout          FpgaReset_ira,

    //==== GBTx ====\\

    // Clocks:
    // Comment: - GbtxClockDes is the clock used by the GBTx I/O registers.
    //            GbtxClockDes is connected to the "Chip global" (Cg) clock network.
    //            GbtxClockDes[1] from L-GEFE goes directly to the FMC connector bypassing the ProAsic3 FPGA.
    input          GbtxClockDesCgP_ik,
    input          GbtxClockDesCgN_ik,

    // Elinks:
    // Comment: In GEFE, the GbtxElinksDio pins are only used as INPUTs.
    input  [15: 0] GbtxElinksDioP_ib16,
    input  [15: 0] GbtxElinksDioN_ib16,
    input  [39:16] GbtxElinksDoutP_ib24,
    input  [39:16] GbtxElinksDoutN_ib24,
    output [39: 0] GbtxElinksDinP_ob40,
    output [39: 0] GbtxElinksDinN_ob40,

    // Slow Control (SC) Elink:
    input          GbtxElinksScOutP_i,
    input          GbtxElinksScOutN_i,
    output         GbtxElinksScInP_o,
    output         GbtxElinksScInN_o,

    // Control:
//    input          GbtxConfigSelV2p5_BUG_i, // Comment: Note!! This signal is a bug in the design and must be set as input or high impedance 'Z'

    //==== Miscellaneous ====\\

    // Crystal oscillator (25MHz):
    // Comment: Osc25Mhz is connected to the "Chip global" (Cg) clock network.
    input          Osc25Mhz_ik,

    //==== Application Module Interface ====\\

    // Resets scheme:
    // Comment: See Microsemi application note AC380.
    output         GeneralReset_oran,

    // GBTx:
    output         GbtxClockDesCg_ok,
    //--
    output [79: 0] DataFromGbtx_ob80,
    input  [79: 0] DataToGbtx_ib80,
    output [ 1: 0] DataFromGbtxSc_ob2,
    input  [ 1: 0] DataToGbtxSc_ib2,

    // Miscellaneous:
    // Comment: - Osc25MhzCg is connected to the "Chip global" (Cg) clock network.
    //          - ClkFeedbackICg is connected to the "Chip global" (Cg) clock network.
    output         Osc25MhzCg_ok
);

//======================================  Declarations  ======================================\\

//==== Variables ====\\

genvar         i;

//==== Wires & Regs ====\\

// Resets scheme:
wire           PorPpr_ra;
wire   [ 1: 0] PorPpr_rand2;

// GBTx:
wire   [15: 0] GbtxElinksDio_b16;
wire   [39:16] GbtxElinksDout_b24;
wire   [39: 0] GbtxElinksDin_b40;
wire           GbtxElinksScOut;
wire           GbtxElinksScIn;

//=======================================  User Logic  =======================================\\

//==== Resets Scheme ====\\

// General reset:
// Comment: See Microsemi application note AC380.
BIBUF_LVCMOS25 i_PorPpr_ibuf (
    .PAD (FpgaReset_ira),
    .D   (1'b0),
    .E   (1'b1),
    .Y   (PorPpr_ra));

DFN1C1 i_PorPpr_d1 (
    .D   (1'b1),
    .CLK (Osc25MhzCg_ok),
    .CLR (PorPpr_ra),
    .Q   (PorPpr_rand2[0]));

DFN1C1 i_PorPpr_d2 (
    .D   (PorPpr_rand2[0]),
    .CLK (Osc25MhzCg_ok),
    .CLR (PorPpr_ra),
    .Q   (PorPpr_rand2[1]));

assign GeneralReset_oran = PorPpr_rand2[1];

//==== GBTx ====\\

// GBTx Elinks clock:
CLKBUF_LVDS i_GbtxElinksDclkCg_ibuf (
    .PADP (GbtxClockDesCgP_ik),
    .PADN (GbtxClockDesCgN_ik),
    .Y    (GbtxClockDesCg_ok));

// GBTx Elinks:

generate for (i=0; i<16; i=i+1) begin: GbtxElinksDio_gen
    INBUF_LVDS i_GbtxElinksDio_ibuf (
        .PADP (GbtxElinksDioP_ib16[i]),
        .PADN (GbtxElinksDioN_ib16[i]),
        .Y    (GbtxElinksDio_b16  [i]));
    //--
    DDR_REG i_GbtxElinksDio_iddr (
        .D   (GbtxElinksDio_b16   [i]),
        .CLK (GbtxClockDesCg_ok),
        .CLR (1'b0),
        .QR  (DataFromGbtx_ob80   [(i*2)+1]),
        .QF  (DataFromGbtx_ob80   [ i*2]));
end endgenerate

generate for (i=0; i<24; i=i+1) begin: GbtxElinksDout_gen
    INBUF_LVDS i_GbtxElinksDout_ibuf (
        .PADP (GbtxElinksDoutP_ib24[16+i]),
        .PADN (GbtxElinksDoutN_ib24[16+i]),
        .Y    (GbtxElinksDout_b24  [16+i]));
    //--
    DDR_REG i_GbtxElinksDout_iddr (
        .D   (GbtxElinksDout_b24   [16+i]),
        .CLK (GbtxClockDesCg_ok),
        .CLR (1'b0),
        .QR  (DataFromGbtx_ob80    [32+((i*2)+1)]),
        .QF  (DataFromGbtx_ob80    [32+ (i*2)]));
end endgenerate

generate for (i=0; i<40; i=i+1) begin: GbtxElinksDin_gen
    DDR_OUT i_GbtxElinksDin_oddr (
        .DR   (DataToGbtx_ib80    [(i*2)+1]),
        .DF   (DataToGbtx_ib80    [ i*2]),
        .CLK  (GbtxClockDesCg_ok),
        .CLR  (1'b0),
        .Q    (GbtxElinksDin_b40  [i]));
    //--
    OUTBUF_LVDS i_GbtxElinksDin_obuf (
        .D    (GbtxElinksDin_b40  [i]),
        .PADP (GbtxElinksDinP_ob40[i]),
        .PADN (GbtxElinksDinN_ob40[i]));
end endgenerate

// GBTx SC Elinks:
INBUF_LVDS i_GbtxElinksScOut_ibuf (
    .PADP (GbtxElinksScOutP_i),
    .PADN (GbtxElinksScOutN_i),
    .Y    (GbtxElinksScOut));
//--
DDR_REG i_GbtxElinksScOut_iddr (
    .D   (GbtxElinksScOut),
    .CLK (GbtxClockDesCg_ok),
    .CLR (1'b0),
    .QR  (DataFromGbtxSc_ob2[1]),
    .QF  (DataFromGbtxSc_ob2[0]));

DDR_OUT i_GbtxElinksScIn_oddr (
    .DR  (DataToGbtxSc_ib2[1]),
    .DF  (DataToGbtxSc_ib2[0]),
    .CLK (GbtxClockDesCg_ok),
    .CLR (1'b0),
    .Q   (GbtxElinksScIn));
//--
OUTBUF_LVDS i_GbtxElinksScIn_obuf (
    .D    (GbtxElinksScIn),
    .PADP (GbtxElinksScInP_o),
    .PADN (GbtxElinksScInN_o));

// Control:
// GbtxConfigSelV2p5_BUG_i // Comment: Note!! This signal is a bug in the design and must be set as input or high impedance 'Z'

//==== Miscellaneous ====\\

// Clocking scheme:
CLKBUF_LVCMOS25 i_Osc25Mhz_ibuf (
    .PAD (Osc25Mhz_ik),
    .Y   (Osc25MhzCg_ok));

endmodule