********************************************************************
                            Global Usage Report
********************************************************************
  
Product: Designer
Release: v11.8
Version: 11.8.0.26
Date: Thu Mar 22 18:45:39 2018
Design Name: SGefeTop  Family: ProASIC3L  Die: A3PE3000L  Package: 896 FBGA
Design State: Post-Layout

The following nets have been routed to a chip global resource:

    Fanout            Name
    ----------------------
    778               Net   : GbtxClockDesCg_k
                      Driver: i_SGefeSystem/i_GbtxElinksDclkCg_ibuf/U0/U1/U_GL
    25                Net   : Osc25MhzCg_k
                      Driver: i_SGefeSystem/i_Osc25Mhz_ibuf/U0/U1/U_GL
    412               Net   : i_SGefeApplication/DataGbtxElinks_qb80_RNIN8TM_0[0]
                      Driver: i_SGefeApplication/DataGbtxElinks_qb80_RNIN8TM_0[0]/U_GL
    5                 Net   : i_SGefeApplication/GbtxElinksDclkCg_k
                      Driver: i_SGefeApplication/i_GbtxElinksDclkCg_ibuf/U0/U1/U_GL
    33                Net   : i_SGefeApplication/ClkFeedbackCg_kb2[1]
                      Driver: i_SGefeApplication/i_ClkFeedback1_Chbuf/U_GL
    33                Net   : i_SGefeApplication/ClkFeedbackCg_kb2[0]
                      Driver: i_SGefeApplication/i_ClkFeedback0_ibuf/U0/U1/U_GL




