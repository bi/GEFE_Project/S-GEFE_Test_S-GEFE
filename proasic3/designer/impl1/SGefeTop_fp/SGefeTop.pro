<project name="SGefeTop" version="1.1">
    <ProjectDirectory>
        E:\work_git\S-GEFE\S-GEFE_Test\proasic3\designer\impl1\SGefeTop_fp
    </ProjectDirectory>
    <View>
        SingleSTAPLView
    </View>
    <LiberoTargetDevice>
        
    </LiberoTargetDevice>
    <LogFile>
        E:\work_git\S-GEFE\S-GEFE_Test\proasic3\designer\impl1\SGefeTop_fp\SGefeTop.log
    </LogFile>
    <SerializationOption>
        Skip
    </SerializationOption>
    <programmer status="enable" type="FlashPro5" revision="UndefRev" connection="usb2.0">
        <name>
            S201YPV2TL
        </name>
        <id>
            S201YPV2TL
        </id>
    </programmer>
    <configuration>
        <Hardware>
            <FlashPro>
                <TCK>
                    4000000
                </TCK>
                <Vpp/>
                <Vpn/>
                <Vddl/>
                <Vdd>
2500                </Vdd>
            </FlashPro>
            <FlashProLite>
                <TCK>
                    4000000
                </TCK>
                <Vpp/>
                <Vpn/>
            </FlashProLite>
            <FlashPro3>
                <TCK>
                    4000000
                </TCK>
                <Vpump/>
                <ClkMode>
                    FreeRunningClk
                </ClkMode>
            </FlashPro3>
            <FlashPro4>
                <TCK>
                    4000000
                </TCK>
                <Vpump/>
                <ClkMode>
                    FreeRunningClk
                </ClkMode>
            </FlashPro4>
            <FlashPro5>
                <TCK>
                    4000000
                </TCK>
                <Vpump/>
                <ClkMode>
                    FreeRunningClk
                </ClkMode>
                <ProgrammingMode>
                    JTAGMode
                </ProgrammingMode>
            </FlashPro5>
        </Hardware>
        <Algo type="PDB">
            <filename>
                E:\work_git\S-GEFE\S-GEFE_Test\proasic3\designer\impl1\SGefeTop.pdb
            </filename>
            <local>
                projectData\SGefeTop.pdb
            </local>
            <SelectedAction>
                PROGRAM
            </SelectedAction>
        </Algo>
    </configuration>
</project>
