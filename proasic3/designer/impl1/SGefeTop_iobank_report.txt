********************************************************************
                            I/O Bank Report
********************************************************************
  
Product: Designer
Release: v11.8
Version: 11.8.0.26
Date: Thu Mar 22 18:45:39 2018
Design Name: SGefeTop  Family: ProASIC3L  Die: A3PE3000L  Package: 896 FBGA


I/O Function:

    Type                                  | w/o register  | w/ register  | w/ DDR register
    --------------------------------------|---------------|--------------|----------------
    Input I/O                             | 130           | 0            | 0
    Output I/O                            | 214           | 0            | 0
    Bidirectional I/O                     | 1             | 0            | 0
    Differential Input I/O Pairs          | 9             | 0            | 41
    Differential Output I/O Pairs         | 2             | 0            | 43

I/O Technology:

                                    |   Voltages    |             I/Os
    --------------------------------|-------|-------|-------|--------|--------------
    I/O Standard(s)                 | Vcci  | Vref  | Input | Output | Bidirectional
    --------------------------------|-------|-------|-------|--------|--------------
    LVCMOS25                        | 2.50v | N/A   | 130   | 214    | 1
    LVDS                            | 2.50v | N/A   | 100   | 90     | 0

I/O Bank Resource Usage:

          |   Voltages    | Single I/Os  | Diff I/O Pairs |        Vref I/Os
          |-------|-------|------|-------|-------|--------|------|-------|----------
          | Vcci  | Vref  | Used | Total | Used  | Total  | Used | Total | Vref Pins
    ------|-------|-------|------|-------|-------|--------|------|-------|----------
    Bank0 | 2.50v | N/A   | 49   | 82    | 14    | 41     | N/A  | N/A   | N/A
    Bank1 | 2.50v | N/A   | 67   | 82    | 1     | 41     | N/A  | N/A   | N/A
    Bank2 | 2.50v | N/A   | 45   | 64    | 4     | 32     | N/A  | N/A   | N/A
    Bank3 | 2.50v | N/A   | 69   | 80    | 3     | 40     | N/A  | N/A   | N/A
    Bank4 | 2.50v | N/A   | 44   | 80    | 0     | 40     | N/A  | N/A   | N/A
    Bank5 | 2.50v | N/A   | 45   | 80    | 16    | 40     | N/A  | N/A   | N/A
    Bank6 | 2.50v | N/A   | 19   | 80    | 28    | 40     | N/A  | N/A   | N/A
    Bank7 | 2.50v | N/A   | 7    | 72    | 29    | 36     | N/A  | N/A   | N/A

I/O Voltage Usage:

       Voltages    |     I/Os
    -------|-------|------|-------
     Vcci  | Vref  | Used | Total
    -------|-------|------|-------
     2.50v | N/A   | 535  | 620

