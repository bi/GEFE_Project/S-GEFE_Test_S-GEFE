All the documents related to the S-GEFE (L-GEFE + C-GEFE) hardware can be accesed through the following links:

L-GEFE:

https://edms.cern.ch/project/EDA-03683

C-GEFE:

https://edms.cern.ch/project/EDA-03684

