- The GEFE project documentation is available at the Open HardWare Repository (OHWR):

  http://www.ohwr.org/projects/gefe

- The GBT project documentation is available through the following link:

  https://espace.cern.ch/GBT-Project/default.aspx


